package com.weichie.yame.aiofficial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


import java.math.BigInteger;

public class FormActivity extends AppCompatActivity {

    private Button submitButton;
    private EditText userName;
    private EditText email;
    private EditText adres;
    private EditText reknr;
    private CheckBox terms;
    private EditText telnr;
    private Button returnToDashboard;
    private TextView seconds;
    private SeekBar slide;
    private TextView euro;
    public Integer bedrag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        submitButton = (Button) findViewById(R.id.btnFormSubmit);
        userName = (EditText) findViewById(R.id.userNameText);
        email = (EditText) findViewById(R.id.txtEmail);
        adres = (EditText) findViewById(R.id.txtAdres);
        telnr = (EditText) findViewById(R.id.txtTel);
        reknr = (EditText) findViewById(R.id.txtBank);
        terms = (CheckBox) findViewById(R.id.checkAlgemeen);
        returnToDashboard = (Button) findViewById(R.id.returnToDashboard);
        seconds = (TextView) findViewById(R.id.txtSeconds);
        slide = (SeekBar) findViewById(R.id.slider);
        euro = (TextView) findViewById(R.id.lblBedrag);



        // Timer starten en kijken hoelang het duurt om formulier in te vullen (data om form beter te maken? ;)

        Timer T = new Timer();
        T.scheduleAtFixedRate(new TimerTask() {
            Integer countSecs = 0;

            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Integer countSecs = 0;
                        seconds.setText(countSecs.toString());
                        countSecs++;
                    }
                });
            }
        }, 1000, 1000);

        slide.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Integer calcBedrag = progress * 10;
                euro.setText( "€ " + calcBedrag.toString() );
                bedrag = calcBedrag;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Integer calcBedrag = seekBar.getProgress() * 10;
                euro.setText( "€ " + calcBedrag.toString() );
                bedrag = calcBedrag;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Integer calcBedrag = seekBar.getProgress() * 10;
                euro.setText( "€ " + calcBedrag.toString() );
                bedrag = calcBedrag;
            }
        });



        returnToDashboard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent dashBoard = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(dashBoard);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer errors = 0;

                if( userName.getText().toString().length() == 0 ) {
                    errors++;
                    userName.setError("Je naam invullen is verplicht");
                }

                if( !email.getText().toString().contains("@") || !email.getText().toString().contains(".") ) {
                    errors++;
                    email.setError("Gelieve een geldig e-mail adres in te vullen");
                }

                if( adres.getText().toString().length() == 0 ) {
                    errors++;
                    adres.setError("Je adres is verplicht");
                }

                if( reknr.getText().toString().length() == 0 || !IbanTest.ibanTest( reknr.getText().toString() ) ) {
                    errors++;
                    reknr.setError("Gelieve een geldig rekening nummer in te vullen");
                }

                if( !terms.isChecked() ) {
                    errors++;
                    terms.setError("Gelieve onze algemene voorwaarden te aanvaarden");
                }

                if( errors == 0 ) {
                    // AlertDialog tonen
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(FormActivity.this);
                    builder1.setMessage(userName.getText() + " is toegevoegd!");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                    // Data wegschrijven - ENKELE STRING
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String database = preferences.getString("database", "");
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("database", database + "%%%" + userName.getText().toString() + ":" + bedrag + ":" + seconds.getText().toString());
                    editor.apply();

                    // Pushbullet naar onze GSM's :D
                    RequestQueue MyRequestQueue = Volley.newRequestQueue(getApplicationContext());

                    String url = "http://yame.be/sendnoti.php?message=" + userName.getText().toString().replaceAll("\\s","+") + "&bedrag=" + bedrag;
                    StringRequest MyStringRequest = new StringRequest(Request.Method.GET,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //This code is executed if the server responds, whether or not the response contains data.
                                    //The String 'response' contains the server's response.
                                }
                            },
                            new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //This code is executed if there is an error.
                                }
                            })
                            {};

                    MyRequestQueue.add(MyStringRequest);

                    // Velden clearen
                    userName.setText("");
                    reknr.setText("");
                    adres.setText("");
                    email.setText("");
                    telnr.setText("");
                    terms.setChecked(false);
                    slide.setProgress(0);
                }

            }
        });
    }
}
