package com.weichie.yame.aiofficial;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button loginButton;
    private Button formButton;
    private TextView sUsername;
    private TextView sNames;
    private TextView sAantal;
    private TextView sBedragen;
    private TextView sTijd;
    private TextView sBedrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        formButton = (Button) findViewById(R.id.btnGoToForm);
        sUsername = (TextView) findViewById(R.id.savedUserName);
        sNames = (TextView) findViewById(R.id.textView2);
        sAantal = (TextView) findViewById(R.id.txtAantal);
        sBedragen = (TextView) findViewById(R.id.txtBevestiging);
        sTijd = (TextView) findViewById(R.id.txtTijd);
        sBedrag = (TextView) findViewById(R.id.txtBedrag);

        formButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent formScreen = new Intent(getApplicationContext(), FormActivity.class);
                startActivity(formScreen);
            }
        });


        // CLEAREN? Uncomment dit :)
        //SharedPreferences.Editor editor = preferences.edit();
        //editor.clear();
        //editor.apply();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String database = preferences.getString("database", "");

        String[] splitDatabase = database.split("%%%");

        String allNames = "";
        String allBedragen = "";
        Integer totaalBedrag = 0;
        Integer totaalTijd = 0;
        Integer i = 0;

        for(int x=splitDatabase.length; x>0; x-- ){
            if( i > 0){
                String[] splitItem = splitDatabase[x].split(":");
                allNames += splitItem[0]+"\n";
                allBedragen += "€ "+ splitItem[1] +"\n";
                totaalBedrag += Integer.parseInt(splitItem[1]);
                totaalTijd += Integer.parseInt(splitItem[2]);
            }
            i++;
        }

        if( i > 1) {
            sNames.setText(allNames);
            Integer aantal = i - 1;
            sAantal.setText(aantal.toString());
            sBedrag.setText( "€ " + totaalBedrag );
            sBedragen.setText(allBedragen);

            Integer gemiddeldeTijd = totaalTijd / aantal;
            sTijd.setText("Gemiddeld " + gemiddeldeTijd.toString() + " seconden.");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
